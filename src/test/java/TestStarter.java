/**
 * @author christ
 * @date 2016/4/15
 */
public class TestStarter extends archer.framework.test.support.starter.TestStarter {

    public static void main(String[] args) {

        new TestStarter().run();
    }
}
