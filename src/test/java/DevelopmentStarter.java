/**
 * @author christ
 * @date 2016/4/15
 */
public class DevelopmentStarter extends archer.framework.test.support.starter.DevelopmentStarter{

    public static void main(String[] args) {

        new DevelopmentStarter().run();
    }
}
