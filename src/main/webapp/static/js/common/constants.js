/**
 * @author Dendi
 * @date 2016/6/14
 */
(function (window) {
// ---------------$Constants 系统常量-------------------
    window.$Config = {
        toast: {hideAfter: 3}

    };
    // ---------------$Constants 系统常量-------------------
    window.$Constants = {
        messages: {
            "toastSuccess": "操作成功",
            "toastError": "操作失败",
            "toastWarning": "操作警告",
            "toastInfo": "操作信息",

            "selectRows":"请选择行数据!",
            "serverInternalError": "系统升级中,请联系管理员",
            "notLogin": "为确保您的操作安全，请重新登录"
        },
        enumKeys:{
            "Y":"Y",
            "N":"N",
            "NEW":"NEW",
            "USED":"USED"
        }
    };
    // ---------------$Enums 枚举-------------------
    window.$Enums = {
        UserStatus: {
            "ENABLE": "启用",
            "DISABLE": "禁用"
        },
        BooleanStatus: {
            "Y": "是",
            "N": "否"
        },
        UserType: {
            "STAFF": "内部用户",
            "CLIENT": "客户端用户"
        },
        ValueType: {
            "STRING": "字符",
            "INTEGER": "整数",
            "BIG_DECIMAL": "金额",
            "DATE": "日期",
            "TIMESTAMP": "时间戳"
        },
        RuleStatus:{
            "NEW":"新建",
            "USED":"启用"
        }
    }

})(window);
