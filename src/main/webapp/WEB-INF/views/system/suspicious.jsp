<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>系统管理-可疑事件</title>
    <link rel="stylesheet" href="${ctx}/static/vendor/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/jsonFormater/jsonFormater.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            可疑事件列表
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">
                可疑事件
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title"></h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="search-form" id="searchForm">
                            <div class="condition with-padding">
                                <div class="row">

                                    <div class="col-xs-2">
                                        <input type="text" id="createTime" name="createTime"
                                               class="form-control"
                                               placeholder="时间">

                                        <input type="text" id="createTimeStart" data-op="ge" name="createTime"
                                               class="form-control" style="display: none">

                                        <input type="text" id="createTimeEnd" data-op="le" name="createTime"
                                               class="form-control" style="display: none">

                                    </div>

                                    <div class="col-xs-2">
                                        <input type="text" name="title" data-op="starts" class="form-control"
                                               placeholder="标题">
                                    </div>
                                    <div>
                                        <button type="button" title="查询" class="btn btn-default btn-flat"
                                                onclick="query()">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- form end -->

                        <div class="archer-table">
                            <!-- action start -->
                            <!-- action end -->

                            <!-- table start -->
                            <table id="masterTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar=".table-action-bar"
                                   data-toggle="table"
                                   data-ajax="load"
                                   data-side-pagination="server"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                   data-pagination="true">
                                <thead>
                                <tr>

                                    <th data-checkbox="true">
                                    </th>
                                    <th data-field="createTime"
                                        data-sortable="true">
                                        时间
                                    </th>
                                    <th data-field="type"
                                        data-sortable="true">
                                        类型
                                    </th>
                                    <th data-field="title">
                                        标题
                                    </th>
                                    <th data-field="level">
                                        等级
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <!-- modal start -->
        <div id="editor" class="edit-form fade modal" data-backdrop="static">
            <div class="modal-dialog" style="margin-top: 200px;width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">异常详情</h4>
                    </div>
                    <div class="modal-body">
                        <div id="contentJson"></div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>
<script src="${ctx}/static/vendor/daterangepicker/moment.min.js"></script>
<script src="${ctx}/static/vendor/daterangepicker/daterangepicker.js"></script>
<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script src="${ctx}/static/vendor/jsonFormater/jsonFormater.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-content">详情</a>',
        ].join('');
    }

    function statusRender(value) {
        return $Enums.UserStatus[value];
    }

    function booleanRender(value) {
        return $Enums.BooleanStatus[value];
    }

    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-content': viewContent,
    };


    //------------------------crud--------------------------

    /**
     * 查询
     */
    function query() {
        masterComponent.reloadTableData();
    }

    /**
     * 新增
     */
    function insert() {
        masterComponent.insert();
        $(".select-enum").initSelect();
    }

    /**
     * 加载数据
     */
    function load(params) {
        masterComponent.loadTableData('${ctx}/core/internal/system/suspicious/findPagedList', params);
    }

    function viewContent(e, value, row) {
        jsonFormater.doFormat(row.content);
        masterComponent.openEditor();
    }


    // 初始化日期范围控件
    archer.utils.initDateRange("#createTime");

    //如果名称相同,可以直接写成_crud.init({});
    var masterComponent = _crud.createCrud({
        scope: '#main',                 //vue scope
        table: '#masterTable',
        editor: '#editor',
        searchForm: '#searchForm'
    });

    masterComponent.init();

    // 初始化json格式化工具
    var jsonFormater = new JsonFormater(
            {
                dom: "#contentJson",
                imgCollapsed: "${ctx}/static/vendor/jsonFormater/Collapsed.gif", //收起的图片路径
                imgExpanded: "${ctx}/static/vendor/jsonFormater/Expanded.gif" //展开的图片路径
            }
    );

</script>
</body>
</html>
