<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>规则管理-规则列表</title>
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            规则列表
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">
                规则管理
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title"></h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="search-form" id="searchForm">
                            <div class="condition with-padding">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <input type="text" name="type" data-op="starts" class="form-control"
                                               placeholder="分组">
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" name="code" data-op="starts" class="form-control"
                                               placeholder="编码">
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" name="val" data-op="starts" class="form-control"
                                               placeholder="值">
                                    </div>
                                    <div>
                                        <button type="button" title="查询" class="btn btn-default btn-flat"
                                                onclick="query()">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- form end -->

                        <div class="archer-table">
                            <!-- action start -->
                            <div class="table-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" onclick="insert();" title="新增">
                                        新增
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="enableAll();" title="删除">
                                        启用
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="removeAll();" title="删除">
                                        删除
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <!-- table start -->
                            <table id="masterTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar=".table-action-bar"
                                   data-toggle="table"
                                   data-ajax="load"
                                   data-sort-name="type"
                                   data-sort-order="asc"
                                   data-side-pagination="server"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                   data-pagination="true">
                                <thead>
                                <tr>
                                    <th data-checkbox="true">
                                    </th>
                                    <th data-field="type"
                                        data-sortable="true">
                                        分组
                                    </th>
                                    <th data-field="code">
                                        编码
                                    </th>
                                    <th data-field="val">
                                        值
                                    </th>
                                    <th data-field="valueType"
                                        data-formatter="valTypeRender">
                                        数据类型
                                    </th>
                                    <th data-field="status"
                                        data-formatter="statusRender">
                                        状态
                                    </th>
                                    <th data-field="remark">
                                        备注
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <!-- modal start -->
        <div id="editor" class="edit-form fade modal" data-backdrop="static">
            <div class="modal-dialog" style="margin-top: 200px;width: 800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">规则信息</h4>
                    </div>
                    <div class="modal-body">


                        <form class="form-horizontal">
                            <div class="box-body">

                                <div class="form-group">
                                    <label required for="type" class="col-sm-2 control-label">分组</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="type" class="form-control" id="type"
                                               v-model="data.type">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="code" class="col-sm-2 control-label">编码</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="code" class="form-control" id="code"
                                               v-model="data.code">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="val" class="col-sm-2 control-label">值</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="val" class="form-control" id="val"
                                               v-model="data.val">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="valueType" class="col-sm-2 control-label">数据类型</label>

                                    <div class="col-sm-10">
                                        <select v-model="data.valueType" id="valueType" name="valueType"
                                                class="form-control select-enum"
                                                data-enum-key="ValueType">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="status" class="col-sm-2 control-label">状态</label>

                                    <div class="col-sm-10">
                                        <select v-model="data.status" id="status" name="status"
                                                class="form-control select-enum"
                                                data-enum-key="RuleStatus">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="remark" class="col-sm-2 control-label">备注</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="remark" class="form-control" id="remark"
                                               v-model="data.remark">
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-default btn-flat pull-right" onclick="store()">保存
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>

<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-update">修改</a>',
            ' <a class="inline-operation inline-enable">启用</a>',
            ' <a class="inline-operation inline-remove">删除</a>'
        ].join('');
    }


    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-update': update,

        'click .inline-enable': enable,

        'click .inline-remove': remove
    };

    /**
     * 数据类型列渲染器
     */
    function valTypeRender(value) {
        return $Enums.ValueType[value];
    }
    /**
     * 规则状态列渲染器
     */
    function statusRender(value) {
        return $Enums.RuleStatus[value];
    }
    //------------------------crud--------------------------

    /**
     * 查询
     */
    function query() {
        masterComponent.reloadTableData();
    }

    /**
     * 新增数据
     */
    function insert() {
        masterComponent.insert({valueType: $Constants.enumKeys.STRING, status: $Constants.enumKeys.NEW});
    }


    /**
     * 加载数据
     */
    function load(params) {
        masterComponent.loadTableData('${ctx}/core/internal/system/rule/findPagedList', params);
    }

    /**
     * 保存数据
     */
    function store() {
        masterComponent.store('${ctx}/core/internal/system/rule/store');
    }

    /**
     * 更新单条数据状态
     */
    function enable(e, value, row) {
        if (canEnable(row)) {
            masterComponent.post('${ctx}/core/internal/system/rule/enable', row,
                    function () {
                        masterComponent.reloadTableData();
                    }
            );
        }
    }

    function canEnable(row) {
        return !archer.toast.invalidSelections({
            rows: [row],
            msg: "该规则已启用",
            invalidStatus: $Constants.enumKeys.USED
        });
    }

    /**
     * 更新数据状态
     */
    function enableAll() {
        var ids = masterComponent.getSelections("id");

        if (!archer.utils.isEmpty(ids)) {
            masterComponent.post('${ctx}/core/internal/system/rule/enableAll', {ids: ids.join(',')},
                    function () {
                        masterComponent.reloadTableData();
                    }
            )
        }
    }

    /**
     * 删除数据
     */
    function remove(e, value, row) {
        if (canRemove(row)) {
            masterComponent.remove('${ctx}/core/internal/system/rule/delete', row);
        }
    }


    function canRemove(row) {
        return !archer.toast.invalidSelections({
            rows: [row],
            msg: "不能删除已启用规则",
            invalidStatus: $Constants.enumKeys.USED
        });
    }

    /**
     * 删除多条数据
     */
    function removeAll() {
        var ids = masterComponent.getSelections("id");
        if (!archer.utils.isEmpty(ids)) {
            masterComponent.remove('${ctx}/core/internal/system/rule/deleteAll', {ids: ids.join(',')});
        }
    }
    /**
     * 修改数据
     */
    function update(e, value, row) {

        masterComponent.update(row);
    }

    //------------------------ init --------------------------
    function initBefore() {
        $(".select-enum").initSelect();
    }

    var masterComponent = _crud.createCrud({
        scope: '#main',                 //vue scope
        table: '#masterTable',
        editor: '#editor',
        searchForm: '#searchForm'
    });

    masterComponent.init(initBefore);

</script>
</body>
</html>
