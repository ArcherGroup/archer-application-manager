<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>角色管理-角色详情</title>
    <link rel="stylesheet" href="${ctx}/static/vendor/ztree/metroStyle/metroStyle.css">
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            角色详情
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li>
                <a href="${ctx}/manager/system/role">角色管理</a>
            </li>
            <li class="active">
                角色详情
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <section class="content-header content-action-bar">
        <div class="bar">
            <div class="btn-group">
                <a href="${ctx}/manager/system/role" class="btn btn-default" title="返回列表界面">
                    列表
                </a>
                <a href="${ctx}/manager/system/role/detail" class="btn btn-default" title="新增">
                    新增
                </a>
                <button type="button" class="btn btn-default" onclick="storeMasterComponentFormData();"
                        title="保存">
                    保存
                </button>
            </div>
        </div>
    </section>

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title">
                            角色信息
                        </h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <form id="masterForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon" required>编码</div>
                                    <input type="text" name="code" class="form-control" v-model="data.code">
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon" required>名称</div>
                                    <input type="text" name="name" class="form-control" v-model="data.name">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">备注</div>
                                    <input type="text" style="min-width: 300px" name="remark" class="form-control"
                                           v-model="data.remark">
                                </div>
                            </div>
                        </form>
                        <!-- form end -->
                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title">用户角色</h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body box-fix-height">

                        <!-- form start -->
                        <!-- form end -->

                        <div class="result-table">
                            <!-- action start -->
                            <div class="table-action-bar" id="role-user-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            onclick="openUnselectedUserRoleComponentEditor();" title="新增">
                                        分配用户
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <!-- table start -->
                            <table id="userRoleTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar="#role-user-action-bar"
                                   data-toggle="table"
                                   data-ajax="loadUserRoleComponentTableData"
                                   data-side-pagination="server"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                   data-pagination="true">
                                <thead>
                                <tr>
                                    <th data-field="loginName"
                                        data-sortable="true">
                                        用户名
                                    </th>
                                    <th data-field="mobile">
                                        手机号
                                    </th>
                                    <th data-field="email">
                                        邮箱
                                    </th>
                                    <th data-field="status"
                                        data-formatter="statusRender">
                                        状态
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>

            <div class="col-md-6">

                <!-- box start -->
                <div class="box box-success ">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title">角色菜单</h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body box-fix-height">

                        <!-- form start -->
                        <!-- form end -->

                        <div>
                            <!-- action start -->
                            <div class="tree-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            onclick="storeRoleMenus();" title="保存">
                                        保存
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <div class="ztree archer-tree" id="archer-tree">

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- modal start -->
        <div id="userRoleEditor" class="fade modal" data-backdrop="static">
            <div class="modal-dialog" style="margin-top: 200px;min-width: 800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">用户信息</h4>
                    </div>
                    <div class="modal-body">


                        <!-- action start -->
                        <div class="table-action-bar" id="user-role-edit-action-bar">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" onclick="assignUsers();" title="保存">
                                    保存
                                </button>
                            </div>
                        </div>
                        <!-- action end -->
                        <table id="unSelectedUserRoleTable"
                               data-unique-id="id"
                               data-query-params-type="archer"
                               data-toolbar="#user-role-edit-action-bar"
                               data-toggle="table"
                               data-ajax="loadUnselectedUserRoleComponentTableData"
                               data-click-to-select="true"
                               data-side-pagination="server"
                               data-show-refresh="true"
                               data-show-columns="true"
                               data-pagination="true">
                            <thead>
                            <tr>
                                <th data-checkbox="true">
                                </th>
                                <th data-field="loginName"
                                    data-sortable="true">
                                    用户名
                                </th>
                                <th data-field="mobile">
                                    手机号
                                </th>
                                <th data-field="email">
                                    邮箱
                                </th>
                                <th data-field="status"
                                    data-formatter="statusRender">
                                    状态
                                </th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>

<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script src="${ctx}/static/vendor/ztree/jquery.ztree.core.min.js"></script>
<script src="${ctx}/static/vendor/ztree/jquery.ztree.excheck.min.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-remove">移除</a>',
        ].join('');
    }

    function statusRender(value) {

        return $Enums.UserStatus[value];
    }

    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-remove': unAssignUsers
    };


    //------------------------master component--------------------------

    /**
     * 加载主表组件form数据
     */
    function loadMasterComponentFormData() {

        if (archer.utils.isEmpty(masterComponent.id())) {
            return;
        }

        masterComponent.loadFormData('${ctx}/core/internal/system/role/find/' + masterComponent.id());
    }

    /**
     * 保存组表组件form数据
     */
    function storeMasterComponentFormData() {

        masterComponent.store('${ctx}/core/internal/system/role/store', function (data) {

            // 刷新未选中用户角色组件
            unSelectedUserRoleComponent.reloadTableData();

            // 刷新菜单树
            initRoleMenuTreeComponent();
        });
    }

    //------------------------user role component--------------------------

    /**
     * 加载用户角色组件table数据
     */
    function loadUserRoleComponentTableData(params) {

        if (archer.utils.isEmpty(masterComponent.id())) {
            params.success([]);
            return;
        }

        userRoleComponent.loadTableData('${ctx}/core/internal/system/role/findSelectedStaffPagedList/' + masterComponent.id(), params);
    }

    /**
     * 移除用户角色关系
     */
    function unAssignUsers(e, value, row) {

        userRoleComponent.remove('${ctx}/core/internal/system/role/unAssignUsers/' + masterComponent.id(), {userIds: row.id}, function (data) {

            // 刷新未选中用户角色组件
            unSelectedUserRoleComponent.reloadTableData();
        });
    }


    //------------------------未选中用户角色组件--------------------------

    /**
     * 新增
     */
    function openUnselectedUserRoleComponentEditor() {

        userRoleComponent.openEditor();
    }

    /**
     * 加载未选中用户组件table数据
     */
    function loadUnselectedUserRoleComponentTableData(params) {

        if (archer.utils.isEmpty(masterComponent.id())) {
            params.success([]);
            return;
        }
        unSelectedUserRoleComponent.loadTableData('${ctx}/core/internal/system/role/findUnselectedStaffPagedList/' + masterComponent.id(), params);
    }

    /**
     * 分配用户角色关系
     */
    function assignUsers() {

        var selected = unSelectedUserRoleComponent.getSelections('id');

        if (archer.utils.isEmpty(selected)) {
            userRoleComponent.hideEditor();
            return;
        }

        userRoleComponent.post(
                '${ctx}/core/internal/system/role/assignUsers/' + masterComponent.id(),
                {userIds: selected.join(',')},
                function () {
                    userRoleComponent.reloadTableData();
                    unSelectedUserRoleComponent.reloadTableData();
                    userRoleComponent.hideEditor();
                }
        );
    }

    /**
     * 初始化角色菜单树
     */
    function initRoleMenuTreeComponent() {

        archer.http.get("${ctx}/core/internal/system/role/findSelectedMenus/" + masterComponent.id(), null, function (data) {
            if (archer.utils.isSuccess(data)) {
                var setting = {
                    check: {
                        enable: true
                    },
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id",
                            pIdKey: "parentId"
                        },
                        key: {
                            url: "xxx"
                        }
                    }

                };
                roleMenuTreeComponent = $.fn.zTree.init($("#archer-tree"), setting, data.resultData);
                roleMenuTreeComponent.expandAll("true");
            }
        });
    }

    /**
     * 保存角色菜单关系
     */
    function storeRoleMenus() {

        var checked = roleMenuTreeComponent.getCheckedNodes(true);

        var ids = [];
        if (!archer.utils.isEmpty(checked)) {
            for (var i = 0; i < checked.length; i++) {
                ids.push(checked[i].id);
            }
        }

        archer.http.post("${ctx}/core/internal/system/role/assignMenus/" + masterComponent.id(), {menuIds: ids.join(',')}, function (data) {
            if (archer.utils.isSuccess(data)) {
                archer.toast.success({msg: data.resultMsg});
                return;
            }

            archer.toast.error({msg: data.resultMsg});
        });
    }

    //------------------------初始化--------------------------

    // 初始化主表组件
    var masterComponent = _crud.createCrud({
        editor: '#masterForm',
        scope: '#masterForm'
    });
    masterComponent.init();

    // 加载主表form数据
    loadMasterComponentFormData();


    // 初始化用户角色组件
    var userRoleComponent = _crud.createCrud({
        scope: '#userRoleEditor',
        table: '#userRoleTable',
        editor: '#userRoleEditor'
    });
    userRoleComponent.init();


    // 初始化未选中用户组件
    var unSelectedUserRoleComponent = _crud.createCrud({
        table: '#unSelectedUserRoleTable'
    });
    unSelectedUserRoleComponent.init();


    // 角色菜单树
    var roleMenuTreeComponent;
    initRoleMenuTreeComponent();

</script>
</body>
</html>
