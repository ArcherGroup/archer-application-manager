<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>角色管理</title>
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            角色列表
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">
                角色管理
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title"></h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="search-form" id="searchForm">
                            <div class="condition with-padding">
                                <div class="row">

                                    <div class="col-xs-2">
                                        <input type="text" name="code" data-op="starts" class="form-control"
                                               placeholder="编码">
                                    </div>

                                    <div class="col-xs-2">
                                        <input type="text" name="name" data-op="starts" class="form-control"
                                               placeholder="名称">
                                    </div>
                                    <div>
                                        <button type="button" title="查询" class="btn btn-default btn-flat"
                                                onclick="query()">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- form end -->

                        <div class="result-table">
                            <!-- action start -->
                            <div class="table-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" onclick="add();" title="新增">
                                        新增
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="removeAll();" title="删除">
                                        删除
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <!-- table start -->
                            <table id="masterTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar=".table-action-bar"
                                   data-toggle="table"
                                   data-ajax="load"
                                   data-side-pagination="server"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                   data-pagination="true">
                                <thead>

                                <tr>
                                    <th data-checkbox="true">
                                    </th>
                                    <th data-field="code"
                                        data-sortable="true">
                                        编码
                                    </th>
                                    <th data-field="name"
                                        data-sortable="true">
                                        名称
                                    </th>
                                    <th data-field="sysData"
                                        data-formatter="booleanRender">
                                        系统数据
                                    </th>
                                    <th data-field="remark">
                                        备注
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <!-- modal start -->
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>

<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-detail">详情</a>',
            ' <a class="inline-operation inline-remove">删除</a>',
        ].join('');
    }


    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-detail': detail,

        'click .inline-remove': remove
    };


    //------------------------crud--------------------------

    /**
     * 查询
     */
    function query() {
        masterComponent.reloadTableData();
    }

    function booleanRender(value) {
        return $Enums.BooleanStatus[value];
    }

    /**
     * 新增
     */
    function add() {
        window.location = "${ctx}/manager/system/role/detail";
    }

    /**
     * 查看所属用户
     */
    function detail(e, value, row) {
        window.location = "${ctx}/manager/system/role/detail?id=" + row.id;
    }

    /**
     * 加载数据
     */
    function load(params) {
        masterComponent.loadTableData('${ctx}/core/internal/system/role/findPagedList', params);
    }

    /**
     * 删除数据
     */
    function remove(e, value, row) {
        masterComponent.remove('${ctx}/core/internal/system/role/delete', row);
    }

    /**
     * 删除多条数据
     */
    function removeAll() {
        var ids = masterComponent.getSelections("id");
        if (!archer.utils.isEmpty(ids)) {
            masterComponent.remove('${ctx}/core/internal/system/role/deleteAll', {ids: ids.join(',')});
        }
    }


    //------------------------初始化--------------------------

    var masterComponent = _crud.createCrud({
        scope: '#main',                 //vue scope
        table: '#masterTable',
        searchForm: '#searchForm'
    });
    masterComponent.init();

</script>
</body>
</html>
