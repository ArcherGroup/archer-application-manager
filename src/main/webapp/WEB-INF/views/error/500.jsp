<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
    response.setStatus(500);
    //记录日志
    Logger logger = LoggerFactory.getLogger("500.jsp");
    logger.error(exception.getMessage(), exception);
%>

<!DOCTYPE html>
<html>
<head>
    <title>500 - 系统内部错误</title>
    <link rel="stylesheet" href="${ctx}/static/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/AdminLTE/css/AdminLTE.min.css">
    <style>

        * {
            font-family: "微软雅黑";
        }

        .title {
            font-size: 150px;
            text-align: center;
        }
    </style>
</head>

<body>
<div class="main">
    <p class="title text-yellow">500</p>

    <p class="text-center text-gray">系统内部出错，请联系管理员</p>

    <p class="text-center"><a href="${ctx}/">点击此处回首页</a></p>
</div>
</body>
</html>
