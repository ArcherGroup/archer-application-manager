<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<%response.setStatus(403);%>

<!DOCTYPE html>
<html>
<head>
    <title>403 - 您没有访问该资源的权限</title>
    <link rel="stylesheet" href="${ctx}/static/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/AdminLTE/css/AdminLTE.min.css">
    <style>

        * {
            font-family: "微软雅黑";
        }

        .title {
            font-size: 150px;
            text-align: center;
        }
    </style>
</head>

<body>
<div class="main">
    <p class="title text-yellow">403</p>

    <p class="text-center text-gray">您没有访问该资源的权限</p>

    <p class="text-center"><a href="${ctx}/">点击此处回首页</a></p>
</div>
</body>
</html>