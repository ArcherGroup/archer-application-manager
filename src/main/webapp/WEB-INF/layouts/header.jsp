<%@ page import="archer.application.manager.utils.SubjectUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="el" uri="/eltag" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>


<header class="main-header">

    <a href="#" class="logo">
        <span class="logo-lg"><b>Archer Manager</b></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <!-- User Image -->
                                            <img src="${ctx}/static/vendor/AdminLTE/img/user2-160x160.jpg"
                                                 class="img-circle"
                                                 alt="User Image">
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <h3>
                                            Design some buttons
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%"
                                                 role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="${ctx}/static/vendor/AdminLTE/img/user2-160x160.jpg" class="user-image"
                             alt="User Image">
                        <span class="hidden-xs"><%=SubjectUtils.getLoginName()%></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="${ctx}/static/vendor/AdminLTE/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image">

                            <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">修改密码</a>
                            </div>
                            <div class="pull-right">
                                <a href="${ctx}/logout" class="btn btn-default btn-flat" onclick="clearStorage()">退出登录</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>


<aside class="main-sidebar">
    <!-- the demo root element -->
    <section class="sidebar" id="sidebarMenu">
        <c:set var="menuData" value="${el:menuData()}" scope="request"/>
        <ul id="menutree" class="sidebar-menu">
            <%@include file="menutree.jsp"%>
        </ul>
    </section>
</aside>

<script>
    var selectedNodes = JSON.parse(sessionStorage.getItem('selectedNodes')) || [];
    var $root = $('#menutree');

    $.each(selectedNodes,function (index, item) {
        $root.find('#' + item).addClass('active');
    });

    function selectNode(menuId){
        selectedNodes = [menuId];
        sessionStorage.setItem('selectedNodes',JSON.stringify(selectedNodes));

        //选择父节点
        selectParentNode($('#' + menuId).parent());
    }

    function selectParentNode(parent){
        //已到根节点
        if (parent.is('#menutree')){
            return;
        }

        //跳过二级节点中的ul元素
        if (parent.is('.treeview-menu')){
            selectParentNode(parent.parent());
            return;
        }

        selectedNodes.push(parent.attr('id'));
        sessionStorage.setItem('selectedNodes',JSON.stringify(selectedNodes));
        selectParentNode(parent.parent());
    }

    function clearStorage() {
        sessionStorage.setItem('selectedNodes','[]');
    }

</script>