package archer.application.manager.utils;

import archer.application.manager.shiro.ShiroRemoteRealm;
import archer.framework.utils.ValidateUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * @author christ
 * @date 2016/7/1
 */
public class SubjectUtils {

    public static Subject getSubject() {

        return SecurityUtils.getSubject();
    }

    public static ShiroRemoteRealm.ShiroUser getPrincipal() {

        if (ValidateUtils.isNotEmpty(getSubject())) {
            return (ShiroRemoteRealm.ShiroUser) getSubject().getPrincipal();
        }

        return ShiroRemoteRealm.ShiroUser.emptyObject;
    }

    public static String getUserId() {

        return getPrincipal().userId;
    }

    public static String getLoginName() {

        return getPrincipal().loginName;
    }
}
