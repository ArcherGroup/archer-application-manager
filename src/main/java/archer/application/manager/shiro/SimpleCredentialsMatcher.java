package archer.application.manager.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;

/**
 * 简单凭证证书对比器
 *
 * @author christ
 */
public class SimpleCredentialsMatcher implements CredentialsMatcher {

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        // 不进行任何处理，此处认为一切已登录用户登录信息都是正确的
        return true;
    }

}
