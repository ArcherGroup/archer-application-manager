package archer.application.manager.service.system;

import archer.application.manager.service.RemoteInvokeService;
import archer.framework.protocol.result.ExecuteResult;
import org.springframework.stereotype.Component;

/**
 * @author christ
 * @date 2016/6/12
 */
@Component
public class StaffService extends RemoteInvokeService {

    /**
     * 登录
     *
     * @param loginName
     * @param password
     * @return
     */
    public ExecuteResult login(String loginName, String password) {

        return remotePost(getRequest(), getResponse(), "/core/internal/system/staff/login", asMap("loginName", loginName, "password", password));
    }

    /**
     * 获得菜单列表
     *
     * @return
     */
    public ExecuteResult findMyMenuList() {

        return remotePost(getRequest(), getResponse(), "/core/internal/system/staff/findMyMenuList");
    }

    /**
     * 获得显示菜单列表
     *
     * @return
     */
    public ExecuteResult findMyDisplayMenuList() {

        return remotePost(getRequest(), getResponse(), "/core/internal/system/staff/findMyDisplayMenuList");
    }

    /**
     * 获得角色列表
     *
     * @return
     */
    public ExecuteResult findMyRoleList() {

        return remotePost(getRequest(), getResponse(), "/core/internal/system/staff/findMyRoleList");
    }
}
