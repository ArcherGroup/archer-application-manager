package archer.application.manager.controller.system;

import archer.framework.client.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/8/18
 */
@Controller
@RequestMapping("manager/system/suspicious")
public class SuspiciousController extends UIController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/suspicious";
    }
}
