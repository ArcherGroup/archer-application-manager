package archer.application.manager.controller.system;

import archer.application.manager.service.system.StaffService;
import archer.framework.client.controller.UIController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/6/13
 */
@Controller
@RequestMapping("manager/system/staff")
public class StaffController extends UIController {

    @Autowired
    StaffService staffService;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/staff";
    }
}
