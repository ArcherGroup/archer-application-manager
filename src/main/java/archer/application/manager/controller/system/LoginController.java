package archer.application.manager.controller.system;

import archer.application.manager.constant.Constant;
import archer.framework.client.controller.UIController;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/6/12
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController extends UIController {

    @RequestMapping(method = RequestMethod.GET)
    public String login() {
        return "system/login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String fail(String username, Model model) {
        model.addAttribute("message", getRequest().getAttribute(Constant.LOGIN_ERROR_INFO));// 异常信息
        model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
        return "system/login";
    }
}
