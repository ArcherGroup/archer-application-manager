package archer.application.manager.controller.system;

import archer.framework.client.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Dendi
 * @date 2016/7/3
 */
@Controller
@RequestMapping("manager/system/rule")
@Transactional
public class RuleController extends UIController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/rule";
    }

}
