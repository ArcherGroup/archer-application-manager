package archer.application.manager.controller.system;

import archer.framework.client.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/6/28
 */
@Controller
@RequestMapping(value = "/error")
public class ErrorController extends UIController {

    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public String error(@PathVariable String code) {
        return "error/" + code;
    }
}
